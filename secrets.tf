##################################################################################
# SECRETS MANAGER (GENERATE and STORE RANDOM PASSWORD)
##################################################################################

resource "random_password" "admin_password" {
  length  = 32
  special = false
}

resource "aws_secretsmanager_secret" "admin_secret" {
  name                    = var.secret_name
  description             = var.secret_description
  recovery_window_in_days = 30
}

resource "aws_secretsmanager_secret_version" "admin_secret_version" {
  secret_id     = aws_secretsmanager_secret.admin_secret.id
  secret_string = random_password.admin_password.result
}
