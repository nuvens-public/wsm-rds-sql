##################################################################################
# SECURITY GROUPS
##################################################################################

resource "aws_security_group" "sg_rds" {
  name        = "SG-RDS-WSM-${var.projectname}"
  description = "Security Group for WSM in RDS"
  vpc_id      = var.vpc_id
  ingress {
    description = "Allow MSSQL in the VPC"
    from_port   = 1433
    to_port     = 1433
    protocol    = "tcp"
    cidr_blocks = [var.ingress_cidr_blocks]
  }
  ingress {
    description = "Internal Network"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [var.ingress_cidr_blocks]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [var.egress_cidr_blocks]
  }
  tags = {
    Name        = "SG-RDS-WSM-${var.projectname}"
    Environment = var.environment
    Project     = var.projectname
    Team        = var.team
  }
}
