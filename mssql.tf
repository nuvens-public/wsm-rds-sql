##################################################################################
# RDS for MS-SQL
##################################################################################

# All available db versions: https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/CHAP_SQLServer.html

locals {
  wsm_db_settings = {
    sql_identifier                  = "rds-wsm"
    sql_allocated_storage           = 20 # in GB
    sql_max_allocated_storage       = 30 # in GB
    sql_apply_immediately           = true
    sql_license_model               = "license-included"
    sql_storage_type                = "gp2"
    sql_engine                      = "sqlserver-ex" # sqlserver-ee (Enterprise), sqlserver-se (Standard) or sqlserver-web
    sql_engine_version              = "15.00.4198.2.v1"
    sql_instance_class              = "db.t3.medium"
    sql_multi_az                    = false
    sql_publicly_accessible         = false
    sql_db_account_username         = "admin"
    sql_db_account_password         = random_password.admin_password.result
    sql_vpc_security_group_ids      = [aws_security_group.sg_rds.id]
    sql_db_subnet_group_name        = "sbg-rds-wsm"
    sql_parameter_group_name        = "pmg-rds-wsm"
    sql_backup_retention_period     = 3 # in days
    sql_skip_final_snapshot         = true
    sql_final_snapshot_identifier   = "rds-wsm-snap"
    sql_maintenance_window          = "Mon:03:00-Mon:04:00"
    sql_allow_major_version_upgrade = true
    sql_auto_minor_version_upgrade  = true
    sql_availability_zone1          = "eu-central-1a"
    sql_availability_zone2          = "eu-central-1b"
    sql_availability_zone3          = "eu-central-1c"
    sql_timezone                    = "GMT Standard Time"
    sql_tags = {
      Name        = "rds-dev-db01"
      Environment = var.environment
      Project     = var.projectname
      Team        = var.team
    }
  }
}

resource "aws_db_subnet_group" "sbg-rds-wsm" {
  name        = "sbg-rds-wsm"
  subnet_ids  = var.priv_subnets
  description = "SQL Express RDS Subnet Group for WSM"
  tags = {
    Name        = "sbg-rds-wsm"
    Environment = var.environment
    Project     = var.projectname
    Team        = var.team
  }
}

resource "aws_db_parameter_group" "pmg-rds-wsm" {
  name        = "pmg-rds-wsm"
  family      = "sqlserver-ex-15.0"
  description = "SQL Express RDS Parameter Group for WSM"
  tags = {
    Name        = "pmg-rds-wsm"
    Environment = var.environment
    Project     = var.projectname
    Team        = var.team
  }
}

resource "aws_db_instance" "wsm_mssql" {
  depends_on                 = [aws_db_subnet_group.sbg-rds-wsm]
  db_subnet_group_name       = local.wsm_db_settings.sql_db_subnet_group_name
  parameter_group_name       = local.wsm_db_settings.sql_parameter_group_name
  identifier                 = local.wsm_db_settings.sql_identifier
  allocated_storage          = local.wsm_db_settings.sql_allocated_storage
  license_model              = local.wsm_db_settings.sql_license_model
  storage_type               = local.wsm_db_settings.sql_storage_type
  engine                     = local.wsm_db_settings.sql_engine
  engine_version             = local.wsm_db_settings.sql_engine_version
  instance_class             = local.wsm_db_settings.sql_instance_class
  multi_az                   = local.wsm_db_settings.sql_multi_az
  availability_zone          = local.wsm_db_settings.sql_availability_zone2
  publicly_accessible        = local.wsm_db_settings.sql_publicly_accessible
  username                   = local.wsm_db_settings.sql_db_account_username
  password                   = local.wsm_db_settings.sql_db_account_password
  vpc_security_group_ids     = local.wsm_db_settings.sql_vpc_security_group_ids
  maintenance_window         = local.wsm_db_settings.sql_maintenance_window
  auto_minor_version_upgrade = local.wsm_db_settings.sql_auto_minor_version_upgrade
  backup_retention_period    = local.wsm_db_settings.sql_backup_retention_period
  skip_final_snapshot        = local.wsm_db_settings.sql_skip_final_snapshot
  final_snapshot_identifier  = local.wsm_db_settings.sql_final_snapshot_identifier
  apply_immediately          = local.wsm_db_settings.sql_apply_immediately
}
