##################################################################################
# GITLAB BACKEND
##################################################################################
#terraform {
#  backend "http" {
#    address         = "https://gitlab.com/api/v4/projects/00000000/terraform/state/tfstate"
#    lock_address    = "https://gitlab.com/api/v4/projects/00000000/terraform/state/tfstate/lock"
#    unlock_address  = "https://gitlab.com/api/v4/projects/00000000/terraform/state/tfstate/lock"
#  }
#}

##################################################################################
# DATA
##################################################################################

data "aws_availability_zones" "available" {}

data "aws_caller_identity" "current" {}

data "aws_region" "current" {}

