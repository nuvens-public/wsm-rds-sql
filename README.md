# WSM-RDS-SQL

Deploy a RDS SQL Database via Terraform


## Description
This project will create all the necessary elements for WorkSpaces Manager to use an RDS Database bases on Microsoft SQL. Basically, it creates the following elements in the AWS account:
1) RDS MS-SQL Database as `rds-wsm`
2) Security Group as `SG-RDS-WSM-${var.projectname}`
3) Secrets Manager object as `Admin-SQL` that allows to connect to the RDS database
4) RDS Subnet Group as `sbg-rds-wsm`
5) RDS Parameter Group as `pmg-rds-wsm`


## Editing variables
There are some variables that can be edited for us to modify the names showed above in the description section in the following file: `mssql.tf`, `security.tf`, `variables.tf`. We can rename the objects to be created using these files. More specifically:

In the `mssql.tf` file, we can manage, per example, the following:
* `sql_identifier`
* `sql_engine`
* `sql_db_subnet_group_name` (recommended not to change it)
* `sql_parameter_group_name` (recommended not to change it)
* `sql_instance_class`
* `sql_multi_az`

Within the `security.tf`, we can just change the name and description of the Security Group to be created for the RDS database. This is tightly associated with the project name in the global variables file.

There is a mandatory change to be done in the `variables.tf` in reference to the `region`, `vpc_id` and `priv_subnets` so that the code can work. Otherwise, it may throw an error similar to:
```yaml
╷
│ Error: Error creating DB Subnet Group: InvalidParameterValue: Some input subnets in :[subnet-b5, subnet-8c, subnet-f1] are invalid.
│       status code: 400, request id: f6187b2f-618e-4748-bc78-345d4f2aaf1e
│ 
│   with aws_db_subnet_group.sbg-rds-wsm,
│   on mssql.tf line 44, in resource "aws_db_subnet_group" "sbg-rds-wsm":
│   44: resource "aws_db_subnet_group" "sbg-rds-wsm" {
│ 
```

Lastly, the `variables.tf` file will include also some interesting resources such as `cidr_block` and `projectname`, and some others worth of customizing like:
* `secret_name`
* `secret_description`
* `ingress_cidr_blocks`
* `kms_keys_name`


## Create a Cloud Shell Session in AWS in the region to deploy
While this process can be integrated with a Source Code Management tool of choice that must also manage the Terraform State, we listed here the steps to deploy from a local AWS CloudShell session:
```bash
sudo yum install -y yum-utils nano
sudo yum-config-manager --add-repo https://rpm.releases.hashicorp.com/AmazonLinux/hashicorp.repo
sudo yum -y install terraform
terraform version
```


## Add the basics to our profile
```bash
nano ~/.bash_profile
```

Add lines:
```yaml
sudo yum install -y yum-utils nano
sudo yum install -y bash-completion > /dev/null 2>&1
```


## If we want to create a persistent directory and move binaries there - don't do it if you have not used it before!!
```bash
mkdir -p $HOME/.local/bin
cd $HOME/.local/bin
sudo mv /usr/bin/nano $HOME/.local/bin
sudo mv /usr/bin/terraform $HOME/.local/bin
```


## Execute Terraform commands
```bash
mkdir -p ~/tf
cd ~/tf
git clone https://gitlab.com/nuvens-public/wsm-rds-sql.git
cd wsm-rds-sql
terraform fmt -list=true -diff -write=false -recursive .
terraform init
terraform plan -out wsm-rds.tfplan
terraform apply "wsm-rds.tfplan"
```


## Destroy after testing
If we are testing the deployment process, we should destroy after a successful action with the command:
```bash
terraform destroy
```

This will ask to actively approve the destroy action. If we want it to be fully automated, use:
```bash
terraform destroy -input=false -auto-approve
```
