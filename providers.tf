##################################################################################
# CONFIGURATION OF PROVIDERS
##################################################################################

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.17.1"
    }
    null      = { version = "~> 3.1.1" }
    random    = { version = "~> 3.3.1" }
    external  = { version = "~> 2.2.2" }
    local     = { version = "~> 2.2.3" }
    cloudinit = { version = "~> 2.2.0" }
  }
}

##################################################################################
# PROVIDERS
##################################################################################
provider "aws" {
  region = var.region
}

# Null: provides constructs that intentionally do nothing – useful in various situations to help orchestrate tricky behavior or work around limitations.
# Random: supports the use of randomness within Terraform configurations. This is a logical provider, which means that it works entirely within Terraform logic, and does not interact with any other services.
# External: a special provider that exists to provide an interface between Terraform and external programs – useful for integrating Terraform with a system for which a first-class provider does not exist.
# Local: used to manage local resources, such as creating files
# CloudInit: exposes the cloudinit_config data source which renders a multipart MIME configuration for use with cloud-init (previously available as the template_cloudinit_config resource in the template provider)

# Template: this provider has been archived. Please use the `templatefile` function or the `Cloudinit` provider instead

