##################################################################################
# VARIABLES GLOBAL
##################################################################################
variable "region" {
  description = "AWS Region to use"
  default     = "eu-central-1"
}

variable "projectname" {
  default = "NUVENS"
}

variable "environment" {
  default = "NON-PRODUCTION"
}

variable "team" {
  default = "DevOps"
}

##################################################################################
# VARIABLES VPC
##################################################################################

variable "vpc_id" {
  default = "vpc-01a01a0a"
}

variable "cidr_block" {
  default = "172.31.0.0/16"
}

variable "ingress_cidr_blocks" {
  default = "172.31.0.0/16"
}

variable "egress_cidr_blocks" {
  default = "0.0.0.0/0"
}

variable "priv_subnets" {
  default = ["subnet-aa01a010", "subnet-aa01a011", "subnet-aa01a012"]
}

variable "subnet_a" {
  default = "subnet-aa01a010"
}

variable "subnet_b" {
  default = "subnet-aa01a011"
}

variable "subnet_c" {
  default = "subnet-aa01a012"
}

##################################################################################
# VARIABLES SECRETS
##################################################################################
variable "secret_name" {
  default = "Admin-SQL"
}

variable "secret_description" {
  default = "Admin Password for RDS Instance"
}

##################################################################################
# VARIABLES ENCRYPTION and KEYPAIRS
##################################################################################
variable "keypair_name" {
  default = "WSM-KEYPAIR"
}

variable "kms_keys_name" {
  default = "arn:aws:kms:eu-central-1:010101010101:key/afdef1ce-0064-441d-b976-57aa185aecd0"
}

##################################################################################
# VARIABLES SESSION MANAGER Profile
##################################################################################
variable "ssm_name" {
  default = "EC2SSM"
}

variable "ssm_arn" {
  default = "arn:aws:iam::010101010101:instance-profile/EC2SSM"
}
