##################################################################################
# OUTPUTS
##################################################################################

output "mssql_id" {
  value = aws_db_instance.wsm_mssql.id
}

output "mssql_address" {
  value = aws_db_instance.wsm_mssql.address
}
